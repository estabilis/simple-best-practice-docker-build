#Following instructions (as of 2020-04-01): https://docs.gitlab.com/ee/ci/docker/using_kaniko.html
#Kaniko docs are here: https://github.com/GoogleContainerTools/kaniko
#While this example shows building to multiple registries for all branches, with a few modifications
#  it can be used to build non-master branches to a "dev" container registry and only build master to 
#  a production container registry.

variables: 
  VERSIONLABELMETHOD: "OnlyIfThisCommitHasVersion" # options: "OnlyIfThisCommitHasVersion","LastVersionTagInGit"
  IMAGE_LABELS: >
    --label org.opencontainers.image.vendor=$GITLAB_USER_NAME
    --label org.opencontainers.image.authors=$GITLAB_USER_NAME
    --label org.opencontainers.image.revision=$CI_COMMIT_SHA
    --label org.opencontainers.image.source=$CI_PROJECT_URL
    --label org.opencontainers.image.documentation=$CI_PROJECT_URL
    --label org.opencontainers.image.licenses=$CI_PROJECT_URL
    --label org.opencontainers.image.url=$CI_PROJECT_URL
    --label vcs-url=$CI_PROJECT_URL
    --label com.gitlab.ci.user=$GITLAB_USER_NAME
    --label com.gitlab.ci.email=$GITLAB_USER_EMAIL
    --label com.gitlab.ci.tagorbranch=$CI_COMMIT_REF_NAME
    --label com.gitlab.ci.pipelineurl=$CI_PIPELINE_URL
    --label com.gitlab.ci.commiturl=$CI_PROJECT_URL/commit/$CI_COMMIT_SHA
    --label com.gitlab.ci.cijoburl=$CI_JOB_URL
    --label com.gitlab.ci.mrurl=$CI_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_ID

get-latest-git-version:
  stage: .pre
  image: 
    name: alpine/git
    entrypoint: [""]
  rules:
    - if: '$VERSIONLABELMETHOD == "LastVersionTagInGit"'    
  script:
    - |
      echo "the google kaniko container does not have git and does not have a packge manager to install it"
      git clone https://github.com/GoogleContainerTools/kaniko.git
      cd kaniko
      echo "$(git describe --abbrev=0 --tags)" > ../VERSIONTAG.txt
      echo "VERSIONTAG.txt contains $(cat ../VERSIONTAG.txt)"
  artifacts:
    paths:
      - VERSIONTAG.txt


.build_with_docker:
  #Hidden job to use as an "extends" template
  stage: build
  image: docker:latest
  services:
    - docker:dind
  script:
    - | 
      echo "Building and shipping image to $CI_REGISTRY_IMAGE"
      docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
      #Build date for opencontainers
      BUILDDATE="'$(date '+%FT%T%z' | sed -E -n 's/(\+[0-9]{2})([0-9]{2})$/\1:\2/p')'" #rfc 3339 date
      IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.created=$BUILDDATE --label build-date=$BUILDDATE"
      #Description for opencontainers
      BUILDTITLE=$(echo $CI_PROJECT_TITLE | tr " " "_")
      IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.title=$BUILDTITLE --label org.opencontainers.image.description=$BUILDTITLE"
      #Add ref.name for opencontainers
      IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.ref.name=$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"

      #Build Version Label and Tag from git tag, LastVersionTagInGit was placed by a previous job artifact
      if [[ "$VERSIONLABELMETHOD" == "LastVersionTagInGit" ]]; then VERSIONLABEL=$(cat VERSIONTAG.txt); fi
      if [[ "$VERSIONLABELMETHOD" == "OnlyIfThisCommitHasVersion" ]]; then VERSIONLABEL=$CI_COMMIT_TAG; fi
      if [[ ! -z "$VERSIONLABEL" ]]; then 
        IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.version=$VERSIONLABEL"
        ADDITIONALTAGLIST="$ADDITIONALTAGLIST $VERSIONLABEL"
      fi
      
      ADDITIONALTAGLIST="$ADDITIONALTAGLIST $CI_COMMIT_REF_NAME $CI_COMMIT_SHORT_SHA"
      if [[ "$CI_COMMIT_REF_NAME" == "master" ]]; then ADDITIONALTAGLIST="$ADDITIONALTAGLIST latest"; fi
      if [[ -n "$ADDITIONALTAGLIST" ]]; then 
        for TAG in $ADDITIONALTAGLIST; do 
          FORMATTEDTAGLIST="${FORMATTEDTAGLIST} --tag $CI_REGISTRY_IMAGE:$TAG "; 
        done; 
      fi

      echo $FORMATTEDTAGLIST
      echo $IMAGE_LABELS

      docker build $IMAGE_LABELS --pull $FORMATTEDTAGLIST -f Dockerfile .
      docker push "$CI_REGISTRY_IMAGE"
      echo "pull the image with the reference: $CI_REGISTRY_IMAGE"

build-for-gitlab-project-registry:
  extends: .build_with_docker
  environment:
    #This is only here for completeness, since there are no CI CD Variables with this scope, the project defaults are used
    # to push to this projects docker registry
    name: push-to-gitlab-project-registry

build-for-docker-hub-registry:
  extends: .build_with_docker
  stage: build
  #This causes CI CD Variables to be taken from GitLab Project definition for variable scope "push-to-docker-hub", 
  # which overrides CI_REGISTRY, CI_REGISTRY_IMAGE, CI_REGISTRY_USER and CI_REGISTRY_PASSWORD
  # these variables with this scope would need to be created if you copy this project to another group or instance
  environment:
    name: push-to-docker-hub
